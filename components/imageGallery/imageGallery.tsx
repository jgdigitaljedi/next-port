import React, { useMemo, useState } from 'react';
import { FaArrowLeft, FaArrowRight, FaDotCircle, FaRegDotCircle } from 'react-icons/fa';
import { Box, Button, Flex, Text } from 'theme-ui';
import ImageBox from '../imageBox';
import { galleryButtonStyle } from './imageGallery.style';
import { whitespaceReplaceWith } from 'stringman-utils';

interface GalleryImage {
  image: any;
  title: string;
}

interface GalleryProps {
  images: GalleryImage[];
}

const ImageGallery: React.FC<GalleryProps> = ({ images }) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [currentImage, setCurrentImage] = useState<GalleryImage>(images[0]);
  const numImages = useMemo(() => {
    return images?.length || 0;
  }, [images]);

  const changeImage = (forward: boolean) => {
    if (forward) {
      const newCurrentIndex = currentIndex === numImages - 1 ? 0 : currentIndex + 1;
      setCurrentIndex(newCurrentIndex);
      setCurrentImage(images[newCurrentIndex]);
    } else {
      const newCurrentIndex = currentIndex === 0 ? numImages - 1 : currentIndex - 1;
      setCurrentIndex(newCurrentIndex);
      setCurrentImage(images[newCurrentIndex]);
    }
  };

  return (
    <Flex sx={{ width: '100%', my: 4, justifyContent: 'center' }}>
      <Box sx={{ boxShadow: 'card' }}>
        <Flex sx={{ justifyContent: 'center', alignItems: 'stretch' }}>
          <Button
            sx={galleryButtonStyle(true)}
            onClick={() => changeImage(false)}
            aria-label="previous image"
          >
            <FaArrowLeft />
          </Button>
          <ImageBox
            imgPath={currentImage.image}
            altText={currentImage.title}
            maxiWidth="800px" // all images were resized to this width, maybe consider upping this a bit
            noBorder={true}
            eager={true}
          />
          <Button
            sx={galleryButtonStyle()}
            onClick={() => changeImage(true)}
            aria-label="next image"
          >
            <FaArrowRight />
          </Button>
        </Flex>
        <Flex
          sx={{
            bg: 'bgLighter',
            flex: 1,
            alignItems: 'center',
            px: 2,
            flexDirection: 'column',
            borderBottomRightRadius: 'small',
            borderBottomLeftRadius: 'small'
          }}
        >
          <Flex
            sx={{
              justifyContent: 'center',
              mb: 2,
              mt: 1
            }}
          >
            {images.map((image: GalleryImage, index: number) => {
              const imageKey = image.title.toLowerCase().replace(/\s+/g, '-');
              if (currentImage.image === image.image) {
                return (
                  <FaDotCircle
                    style={{
                      marginRight: numImages - 1 === index ? 0 : '.5rem'
                    }}
                    key={imageKey}
                    aria-label="current image"
                  />
                );
              }
              return (
                <FaRegDotCircle
                  onClick={() => {
                    setCurrentIndex(index);
                    setCurrentImage(image);
                  }}
                  style={{ marginRight: numImages - 1 === index ? 0 : '.5rem', cursor: 'pointer' }}
                  key={imageKey}
                  aria-label={`go to image ${index + 1}`}
                />
              );
            })}
          </Flex>
          <Text>{currentImage.title}</Text>
        </Flex>
      </Box>
    </Flex>
  );
};

export default ImageGallery;
