import { GetStaticProps, NextPage } from 'next';
import { getPostsForSection } from '../../lib/getMdx';
import { IndexProps } from '../../types/post';
import SectionLayout from '../../components/blog/sectionLayout';
import { Box } from 'theme-ui';
import Head from 'next/head';

const CodingIndex: NextPage<IndexProps> = ({ posts }) => {
  const topText = `To me, writing code is a lifelong endeavor. I wrote BASIC and C++ long before I ever touched a website. Since becoming a web developer, I’ve used everything from Angular, React, Vue, Node, and more. I plan to keep exploring languages and frameworks, as I’ve had my eyes on Rust (for backend and systems stuff), React Native (for cross compiling to mobile), and Svelte (because I'm curious) for a while now. If I feel like writing about something coding related, you will find it here!`;
  return (
    <Box>
      <Head>
        <title>Joey Gauthier's coding blog</title>
        <meta name="description" content="Joey Gauthier's coding blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SectionLayout posts={posts} section="coding" topText={topText} />
    </Box>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const posts = getPostsForSection('coding');

  return {
    props: { posts }
  };
};

export default CodingIndex;
