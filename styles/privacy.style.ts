import { ThemeUIStyleObject } from 'theme-ui';

export const privacySectionStyle: ThemeUIStyleObject = {
  my: 4,
  'p, ul': {
    mb: 3
  },
  h2: {
    mb: 2
  },
  borderBottomStyle: 'solid',
  borderBottomColor: 'primary',
  borderBottomWidth: '1px'
};
