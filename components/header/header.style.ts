import { ThemeUIStyleObject } from 'theme-ui';

export const headerStyle: ThemeUIStyleObject = {
  display: 'flex',
  flexDirection: 'column',
  position: 'relative',
  '@media print': {
    display: 'none'
  }
};

export const headerInnerStyle: ThemeUIStyleObject = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between'
};

export const headerLogoStyle: ThemeUIStyleObject = {
  display: 'flex',
  flex: 1,
  '::after': {
    content: '""',
    background:
      'repeating-linear-gradient(90deg, #23B0FF, #23B0FF 2px, transparent 0, transparent 10px)',
    display: 'block',
    width: '100%',
    right: '10px'
  }
};

export const headerLink: ThemeUIStyleObject = {
  flex: '0 0 auto',
  maxWidth: '100%',
  textDecoration: 'none'
};

export const logoStyle = (pathname: string): ThemeUIStyleObject => {
  const isHome = pathname === '/';
  return {
    display: 'flex',
    alignItems: 'center',
    textDecoration: 'none',
    background: isHome ? 'primary' : 'muted',
    color: isHome ? 'background' : 'text',
    p: '5px 10px',
    fontWeight: 'bold',
    '&:hover': {
      bg: isHome ? 'muted' : 'primary',
      color: isHome ? 'text' : 'background'
    }
  };
};

export const menuTriggerStyle: ThemeUIStyleObject = {
  color: 'primary',
  border: '2px solid',
  ml: '10px',
  height: '100%',
  p: '3px 8px',
  position: 'relative',
  display: 'none',
  bg: 'transparent',
  cursor: 'pointer',
  '@media screen and (max-width: 48rem)': {
    display: 'block'
  }
};

export const navbarStyle: ThemeUIStyleObject = {
  pt: 3,
  display: 'flex',
  flex: 1,
  justifyContent: 'center',
  '@media screen and (max-width: 48rem)': {
    display: 'none'
  },
  mb: '1.5rem'
};
