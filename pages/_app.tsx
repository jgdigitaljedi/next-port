/** @jsxImportSource theme-ui */
import type { AppProps } from 'next/app';
import { Box, ThemeProvider } from 'theme-ui';
import theme from '../theme';
import Header from '../components/header';
import '../public/static/fonts.css';
import ScrollToTop from '../components/scrollToTop';
import Footer from '../components/footer';
import '../styles/codeStyle.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <>
        <Box
          sx={{
            p: [3, 3, 4, 4, 4, 4]
          }}
        >
          <Header />
          <Box sx={{ minHeight: '70vh' }}>
            <Component {...pageProps} />
          </Box>
          <ScrollToTop />
        </Box>
      </>
      <Footer />
    </ThemeProvider>
  );
}

export default MyApp;
