import type { NextPage } from 'next';
import Head from 'next/head';
import { Box } from 'theme-ui';
import PostTitle from '../components/postTitle/postTitle';
import { containerStyle } from '../styles/global.style';
import Me8Bit from '../public/static/me_8bit_scanlines.jpg';
import ImageBox from '../components/imageBox';

const Home: NextPage = () => {
  return (
    <Box>
      <Head>
        <title>Joey Gauthier</title>
        <meta name="description" content="Joey Gauthier's portfolio" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Box as="main" sx={containerStyle}>
        <PostTitle title="Joey Gauthier" />
        <ImageBox
          altText="pixelated image of Joey with scanlines"
          imgPath={Me8Bit}
          containerStyle={{ mt: 4, maxWidth: '48rem' }}
        />
        <Box sx={{ mt: 4, display: 'flex', justifyContent: 'center' }}>
          <Box as="p">
            Hi! I'm Joey and I'm a software engineer. Welcome to my super simple portfolio and blog!
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Home;
