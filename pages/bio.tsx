import React from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import { Box, Flex, Heading, Link } from 'theme-ui';
import PostTitle from '../components/postTitle/postTitle';
import { containerStyle } from '../styles/global.style';
import makerSquare from '../public/static/makersquare.jpg';
import appleIIe from '../public/static/appleIIe.jpg';
import cLogo from '../public/static/c-logo.png';
import meRocking from '../public/static/meRockingOut.jpg';
import toolbox from '../public/static/toolbox.png';
import ImageBox from '../components/imageBox';
import Divider from '../components/divider';
import { JobProps, jobs } from '../util/constants';
import JobCard from '../components/jobCard';
import { FaGithubSquare, FaGitlab, FaLinkedin } from 'react-icons/fa';
import theme from '../theme';
import {
  jobsContainerStyle,
  linkContainerStyle,
  linkIconStyle,
  linkStyle,
  paraMargin,
  paraMarginBottom
} from '../styles/bio.style';

const Bio: NextPage = () => {
  return (
    <Box>
      <Head>
        <title>Joey Gauthier | About</title>
        <meta name="description" content="About Joey Gauthier" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Box as="main" sx={containerStyle}>
        <PostTitle title="About Joey" />
        <Box>
          <Heading as="h2">Index</Heading>
          <Box as="ol">
            <Box as="li">
              <Link href="#bio">Bio</Link>
            </Box>
            <Box as="li">
              <Link href="#career">Career</Link>
            </Box>
            <Box as="li">
              <Link href="#links">Links</Link>
            </Box>
          </Box>
        </Box>
        <Divider />
        <Box as="article" sx={paraMargin} id="bio">
          <Heading as="h2">Bio</Heading>
          <ImageBox altText="an Apple IIe" imgPath={appleIIe} containerStyle={{ my: 4 }} />
          <Box as="p" sx={paraMarginBottom}>
            Around the age of 9, I started writing BASIC code on an old Apple IIC that my uncle gave
            me. He included a container of floppy disks filled with games and stacks of Compute!
            magazines. After growing tired of the games I already had on floppy, I cracked open one
            of those magazines out of boredom and realized they contained pages of code that, if I
            had the patience to type out, would give me new games to play. After running through
            several of these, I picked up on some of the BASIC programming language. I started
            experimenting and realized I could change the colors, speed, shapes, etc. in these
            simple games. Before too long, I was making my own games from scratch. This is where my
            interest in programming began.
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            Once in high school, I took every computer class available. This was during the 90s, so
            computer classes were finally being offered at most schools, which was good news to me!
            I remember taking a multimedia class where I learned the basics of layouts and graphic
            design. Finally, my school offered a beginner programming class where I was introduced
            to the C++ programming language and the basics of data structures. In my spare time, I
            played guitar in a nu-metal band with my buddies, so the idea of writing code for fun
            quickly fizzled out, but my programming class made sure I kept learning.
          </Box>
          <ImageBox altText="C++ logo" imgPath={cLogo} containerStyle={{ my: 4 }} />
          <Box as="p" sx={paraMarginBottom}>
            Eventually, I started college as a computer science major with a minor in math. During
            that period, I continued to play guitar in my band during what little spare time I had
            to devote to it. After two semesters of college, the band was really seeing some
            success. It was a tough decision to make, but I stopped attending college to devote all
            the time I had to pursuing my dream of becoming a musician. At least I got 2 more
            semesters of data structures and working in C++ under my belt!
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            After a few short years, the band had gotten quite popular in the area in which we
            lived. We decided that, rather than being a big fish in a small pond, it was time to
            move to a much larger city with a richer music scene to see what sort of success we
            could achieve there. We scouted a few cities, but we ultimately moved to Austin, TX.
          </Box>
          <ImageBox
            altText="Joey playing guitar at a live show"
            imgPath={meRocking}
            containerStyle={{ my: 4 }}
          />
          <Box as="p" sx={paraMarginBottom}>
            The next few years were filled with hundreds of live shows, recording five albums, going
            on tours, and opening shows for some of the biggest names in the business. It was one of
            the most interesting and fun-filled periods of my life! I eventually came to realize how
            brutal the music industry truly was and had to give serious consideration to the idea
            that I may never be a rockstar. After seven amazing years of playing guitar for
            Down-Stares, I finally decided it was time to hang it up and try to figure out what it
            meant to live a “normal” life.
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            My wife and I moved back to our hometown and attempted to figure out what we wanted to
            be when we “grew up”. It took a few years for me to find my footing. I learned I was a
            terrible server. I tried my hand as a customer service rep at a Cingular (now AT&T) call
            center. Near the end of my time with Cingular, I started a PC repair business to
            generate a little side money. Eventually, I finally got my first “decent” job with a
            company named Partnership Broadband which provided wireless internet to rural areas. The
            job required me to get on customers’ roofs to install the wireless receivers, then drop
            the cable through their walls so I could connect their routers. The worst part of this
            job was that I also had to climb towers to install and adjust our radio equipment to
            expand and improve our coverage areas. Let me tell you, there is nothing like being
            hundreds of feet up in the air on a tower in the middle of nowhere by yourself! After
            several months of avoiding potential accidental death from a long fall, I got an
            interesting call from a recruiter for IBM that wanted to know if I was interested in
            working as an IT guy for them. I guess that my experience from my PC repair company and
            the basic networking knowledge I picked up working for Partnership Broadband made me
            qualified in their eyes. The recruiter made sure to let me know on that call that the
            position was proving difficult to fill, as they had burned through seven people in the
            past six months. Regardless, I was ready to have my feet back onto solid ground, so I
            took the gig and lived the motto “fake it til you make it”. The funniest part of the
            entire ordeal was that I was to represent IBM in an IT service contract they had with
            AT&T, and my home office would be the very call center at which I used to be a customer
            service rep!
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            The first few months in this position were nerve-racking. I knew little about Active
            Directory, network security, etc, but I was expected to know those things going into the
            position. Thankfully, I’m a pretty fast learner and could figure these things out as
            needed. It took little time for me to go from the nervous IT guy who didn’t know what he
            was doing to becoming one of the top performers on the account. I was closing tickets
            left and right! I eventually noticed that, over time, a lot of the same issues would
            show up. This prompted me to explore the idea of fixing these issues remotely using .vbs
            and .bat scripts. The next thing I knew, I had massive folders of these scripts and
            could do almost anything remotely as long as the machine in question hadn’t fallen off
            the domain. I was coding again...sort of!
          </Box>
          <ImageBox
            altText="screenshot of the generic version of my Toolbox app with IBM specific functionality removed"
            imgPath={toolbox}
            containerStyle={{ my: 4 }}
          />
          <Box as="p" sx={paraMarginBottom}>
            At one point, my collection of scripts had grown so large that it sometimes took a few
            minutes to explore them before I found the one I needed. Couple that with the fact that
            I was always having to edit each script to target different machines via IP address or
            hostname, and I realized that a desktop app would serve me much better. I took some time
            to research how to approach creating such an app, but I eventually devised a plan. After
            several months of working on the app in my spare time, I created a desktop app that
            served as a GUI for my collection of scripts. I then changed each script to take
            arguments for the IP address or hostname, and I added a section to my app where I could
            simply enter a list of machines to target with my scripts. Now my job was simple: I
            dropped in my list of machines to target, selected which script to run on all of them,
            hit the “Go!” button, and sit back and watch the terminal output to make sure there were
            no failures. Tickets were being closed dozens at a time. My manager at IBM eventually
            asked me how I was so fast at closing so many issues, so I told him about my app. He
            alerted his boss, who then reached out to me to explain how to publish an app within the
            company. My little app had been published!
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            After 6.5 years in this position with IBM and having an app published, I finally had the
            thought “why am I not just working as a developer full-time?” I spent some time trying
            to figure out the best way to approach this, and I soon discovered coding boot camps. I
            was somewhat opposed to the idea of being a web developer at first, but it eventually
            occurred to me that the future was on the web. After looking into a few of these camps
            and reaching out to the ones in which I was most interested, I finally applied to
            MakerSquare, and I was accepted! Now I had to figure out the logistics of pulling this
            off. At this point, I was the father of a 5-year-old and I couldn’t see dragging him and
            my wife to Austin so I could be unemployed and spend crazy amounts of hours each day in
            classes or working on projects. I eventually sold off most of what I owned, moved my
            wife and kiddo in with my father and his wife, threw the rest of our belongings into
            storage, loaded my car with the bare minimum of what I needed to survive, and drove down
            to Austin to live in my friend’s spare bedroom while I attended MakerSquare. I didn’t
            know if this would pay off, but I also understood that big risks sometimes have big
            rewards.
          </Box>
          <ImageBox altText="MakerSquare logo" imgPath={makerSquare} containerStyle={{ my: 4 }} />
          <Box as="p" sx={paraMarginBottom}>
            For three months, I lived, breathed, and ate code. MakerSquare had me throwing up new
            projects using Ruby and JavaScript at speeds I never thought possible. Seeing my family
            wasn’t a luxury I would have for a while, but I honestly didn’t have time to do anything
            but learn about web development during this period. The rate at which my classmates and
            I were learning the modern concepts of web development was astounding! I finally got to
            see my family as they visited the open house that MakerSquare had at the end of our
            three months of immersion. During our last week at MakerSquare, we all developed
            something that we could show off to potential employers who would also be at this open
            house event. It was a pretty sweet ordeal as, not only did I get to see my family, who I
            missed dearly by this point, but the Director from my future employer stopped to look at
            my project and chatted with me at some depth about it. Exactly ten days after that open
            house event, I signed my first offer letter for a junior developer position! My insane
            risk paid off in spades!
          </Box>
          <Box as="p" sx={paraMarginBottom}>
            It’s been almost a full decade since that day, and I’ve been writing code ever since. I
            used to tell adults I wanted to be a programmer when I grew up, and I started down that
            path as well. After many years of chasing another dream and having a handful of other
            occupations, I went full-circle and ended up doing what I always used to say I wanted to
            do after all. I guess it was always meant to be!
          </Box>
        </Box>
        <Divider />
        <Box sx={paraMargin} id="career">
          <Heading as="h2">Career</Heading>
          <Box as="p" sx={paraMargin}>
            I’m no spring chicken these days, so I’ve had a handful of jobs over the years. That
            being said, I’ll only list the most recent jobs that apply to my current career.
          </Box>
          <Flex sx={jobsContainerStyle}>
            {jobs.map((job: JobProps) => {
              return <JobCard job={job} key={job.company} />;
            })}
          </Flex>
        </Box>
        <Divider />
        <Box id="links" sx={paraMargin}>
          <Heading as="h2">Links</Heading>
          <Box as="p" sx={paraMargin}>
            Personally, I find social media to be manipulative, censorious, incredibly invasive of
            people's privacy, and just plain bad for your overall mental state. That said, I deleted
            all of my social media accounts years ago and never looked back! I guess some people
            might refer to LinkedIn as a social network, but I keep a LinkedIn profile as a tool to
            find my next amazing job. That being said, below you’ll find a link to my LinkedIn page
            as well as some other relevant links.
          </Box>
          <Box as="p" sx={paraMargin}>
            You might also be wondering, “why does he have a GitHub and a GitLab”? I’d be happy to
            tell you. I, like most other developers, used GitHub exclusively for a long while. After
            Microsoft acquired GitHub, I continued using it but started paying attention to news
            surrounding what changes they were making. There weren’t many changes at first, but
            later it came out that Microsoft was training their AI using your code if it wasn’t in a
            private repo. They also started sending more marketing-style emails, of which no one is
            a fan. I’m not a fan of Microsoft as a company, nor am I a fan of big tech companies
            abusing their power in this way. It feels like Microsoft is basically saying they own
            your code. At that time, I started a GitLab account and began opening new repos in
            GitLab while continuing to use GitHub for my old projects that were already there.
          </Box>
          <Box sx={paraMargin}>
            <Flex sx={linkContainerStyle}>
              <FaLinkedin size={24} color={linkIconStyle} />
              <Link
                sx={linkStyle}
                href="https://www.linkedin.com/in/paulgauthier81"
                target="_blank"
              >
                LinkedIn
              </Link>
            </Flex>
            <Flex sx={linkContainerStyle}>
              <FaGithubSquare size={24} color={linkIconStyle} />
              <Link sx={linkStyle} href="https://github.com/jgdigitaljedi" target="_blank">
                GitHub
              </Link>
            </Flex>
            <Flex sx={linkContainerStyle}>
              <FaGitlab size={24} color={linkIconStyle} />
              <Link sx={linkStyle} href="https://gitlab.com/jgdigitaljedi" target="_blank">
                GitLab
              </Link>
            </Flex>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Bio;
