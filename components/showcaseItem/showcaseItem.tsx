import React from 'react';
import { Box, Card, Heading, Link, Paragraph, Text } from 'theme-ui';
import { LinkType, ShowCase } from '../../util/showcaseProjects';
import { FaGithub, FaGithubSquare, FaGitlab, FaGlobe, FaNpm } from 'react-icons/fa';
import Divider from '../divider';
import { linkIconStyle } from './showcaseItem.style';

interface ShowcaseItemProps {
  project: ShowCase;
}

const ShowcaseItem: React.FC<ShowcaseItemProps> = ({ project }) => {
  const getLinkIcon = () => {
    switch (project.linkType) {
      case LinkType.Npm:
        return <FaNpm fontSize="2rem" />;
      case LinkType.GitHub:
        return <FaGithub fontSize="2rem" />;
      case LinkType.GitLab:
        return <FaGitlab fontSize="2rem" />;
      case LinkType.Site:
      default:
        return <FaGlobe fontSize="2rem" />;
    }
  };

  return (
    <Card variant="project" sx={{ maxWidth: '32rem', m: 2 }}>
      <Heading as="h3" sx={{ color: 'primary' }}>
        {project.title}
      </Heading>
      <Paragraph sx={{ mt: 4 }}>{project.description}</Paragraph>
      <Divider />
      <Box>
        <Text sx={{ fontWeight: 'bold', color: 'primary' }}>Tech: </Text>
        {project.tech.join(', ')}
      </Box>
      <Divider />
      {project.link && (
        <Box sx={linkIconStyle}>
          <Link
            variant="normal"
            target="_blank"
            href={project.link}
            aria-label={`Go to ${project.title}`}
          >
            {getLinkIcon()}
          </Link>
        </Box>
      )}
    </Card>
  );
};

export default ShowcaseItem;
