export type PostType = {
  date?: string;
  description?: string;
  image?: string;
  slug: string;
  title: string;
  category: string;
  altText?: string;
  tags?: string[];
  section: Section;
};

export type IndexProps = {
  posts: PostType[];
};

export type Section = 'coding' | 'gaming';

export type MainBlogProps = {
  recentCoding: PostType;
  recentGaming: PostType;
};

interface NextPrevItem {
  data: PostType;
  slug: string;
}

export interface NextPrev {
  prev: NextPrevItem | null;
  next: NextPrevItem | null;
}
