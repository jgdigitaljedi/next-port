import { readdirSync } from 'fs';
import { join } from 'path';
import { Section } from '../types/post';
import { blogSubdirs } from './constants';

// POSTS_PATH is useful when you want to get the path to a specific file
export const POSTS_PATH = join(process.cwd(), 'blog');

const postSubs = (): string[] => {
  const subs: string[][] = blogSubdirs.map((subdir: string) => {
    return readdirSync(join(POSTS_PATH, subdir)).map((file: string) => `${subdir}/${file}`);
  });
  // @ts-ignore
  return [].concat.apply([], subs);
};

// postFilePaths is the list of all mdx files inside the POSTS_PATH directory
export const postFilePaths = postSubs()
  // Only include md(x) files
  .filter((path) => /\.mdx?$/.test(path));

export const getSectionPosts = (section: Section) => {
  return readdirSync(join(POSTS_PATH, section))
    .map((file: string) => `${file}`)
    .filter((path) => /\.mdx?$/.test(path));
};
