/** @jsxImportSource theme-ui */
import React, { useRef, useState } from 'react';
import { Box, Link, Button } from 'theme-ui';
import { useRouter } from 'next/router';
import {
  headerStyle,
  headerInnerStyle,
  headerLogoStyle,
  logoStyle,
  headerLink,
  menuTriggerStyle,
  navbarStyle
} from './header.style';
import Menu from './menu';
import NavItem from './navItem';

const Header: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);
  const router = useRouter();
  const buttonRef = useRef(null);

  const onMenuClick = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  return (
    <Box sx={headerStyle} role="banner">
      <Box sx={headerInnerStyle}>
        <Box sx={headerLogoStyle}>
          <Link href="/" sx={headerLink}>
            <Box sx={logoStyle(router.pathname)}>{'> Joey G'}</Box>
          </Link>
        </Box>
        <Button sx={menuTriggerStyle} type="button" onClick={onMenuClick} ref={buttonRef}>
          menu
        </Button>
      </Box>
      <Box sx={navbarStyle} role="navigation">
        <NavItem currentPath={router.pathname} />
      </Box>
      {isMenuOpen && <Menu clickCb={() => setIsMenuOpen(false)} buttonRef={buttonRef} />}
    </Box>
  );
};

export default Header;
