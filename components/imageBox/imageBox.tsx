import Image from 'next/image';
import React from 'react';
import { Box, ThemeUIStyleObject } from 'theme-ui';
import { imageStyle } from '../../styles/global.style';
import ImagePlaceholder from '../../public/static/terminalImagePlaceholder.png';

interface ImageBoxProps {
  imgPath: any;
  altText: string;
  maxiWidth?: string;
  eager?: true;
  containerStyle?: ThemeUIStyleObject;
  noBorder?: boolean;
}

const ImageBox: React.FC<ImageBoxProps> = ({
  imgPath,
  altText,
  maxiWidth,
  eager,
  containerStyle,
  noBorder
}) => {
  const maxWidthCalc = maxiWidth || '40rem';
  return (
    <Box sx={{ display: 'flex', justifyContent: 'center' }}>
      <Box sx={{ ...imageStyle(noBorder), maxWidth: maxWidthCalc, ...(containerStyle || {}) }}>
        <Image
          layout="fill"
          objectFit="contain"
          src={imgPath || ImagePlaceholder}
          alt={altText}
          loading={eager ? 'eager' : 'lazy'}
          unoptimized={eager ? true : false}
          placeholder={typeof imgPath === 'string' ? undefined : 'blur'}
        />
      </Box>
    </Box>
  );
};

export default ImageBox;
