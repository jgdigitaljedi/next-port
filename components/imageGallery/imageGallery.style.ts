import { ThemeUIStyleObject } from 'theme-ui';

export const galleryButtonStyle = (left?: boolean): ThemeUIStyleObject => {
  const leftBorder = {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  };
  const rightBorder = {
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  };
  return {
    width: 'auto',
    alignSelf: 'stretch',
    cursor: 'pointer',
    ...(left ? leftBorder : rightBorder)
  };
};
