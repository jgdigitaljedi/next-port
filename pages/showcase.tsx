import React from 'react';
import { NextPage } from 'next';
import { Box, Flex, Heading, Paragraph } from 'theme-ui';
import { containerStyle } from '../styles/global.style';
import Head from 'next/head';
import PostTitle from '../components/postTitle/postTitle';
import Divider from '../components/divider';
import { ShowCase, showcaseProjects } from '../util/showcaseProjects';
import ShowcaseItem from '../components/showcaseItem';

const Showcase: NextPage = () => {
  /** projects
   * tinfoilmylife?
   * previous portfolios?
   */
  return (
    <Box as="main">
      <Head>
        <title>Joey Gauthier | Showcase</title>
        <meta name="description" content="Joey's Showcase" />
        <meta name="keywords" content="Joey Gauthier, portfolio, showcase" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Box sx={containerStyle}>
        <PostTitle title="Joey's showcase" />
        <Box>
          Over the years, I’ve built many cool things for my employers that aren’t available to view
          publicly. While working at Kapsch, I built several dashboards for organizations like MTA
          and CTRMA that allow them to monitor the traffic flow and camera systems for their toll
          roads and manually set the prices of the tolls if needed. During my time at R4, I built
          several dashboards for companies that allow them to visualize their mountains of data in
          useful in interesting ways. None of these projects are public facing, so there is
          unfortunately no way to show off my contributions.
        </Box>
        <Paragraph sx={{ mt: 4 }}>
          I have touched many surfaces of Indeed’s website you can view if you log in as an employer
          of a small to medium-sized business. If you’ve ever created a campaign on Indeed, my last
          product team created that entire experience, and I made many contributions to the UI. The
          jobs list for employers is something I’ve contributed to quite a bit as my team worked
          with that team fairly often to create entry points for the campaign experience and
          integrate campaigns data into that list. There are other parts of Indeed’s site to which
          I’ve made contributions as well, but these days I’m more focused on building and
          maintaining tools to help developers across all product teams write cleaner code, avoid
          introducing new a11y issues, catch any untranslated strings to avoid serving a string in
          the incorrect language for a user, and more.
        </Paragraph>
        <Paragraph sx={{ mt: 4 }}>
          Since my work history doesn’t lend itself well to the idea of showcasing that work in a
          portfolio, I’ve listed some personal projects of note below that you can view (or at least
          view the repo).
        </Paragraph>
      </Box>
      <Divider />
      <Box sx={{ mt: 4 }}>
        <Heading as="h2">Personal Projects</Heading>
        <Flex sx={{ flexWrap: 'wrap', justifyContent: 'center', mt: 2 }}>
          {showcaseProjects.map((item: ShowCase) => {
            return <ShowcaseItem project={item} key={item.title.replace(/\s+/g, '-')} />;
          })}
        </Flex>
      </Box>
    </Box>
  );
};

export default Showcase;
