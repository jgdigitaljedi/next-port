import { ThemeUIStyleObject } from 'theme-ui';

export const cardStyle = (image?: string): ThemeUIStyleObject => {
  const baseStyle = {
    cursor: 'pointer',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center'
  };
  if (image) {
    return {
      ...baseStyle,
      background: `url(${image}), rgb(29, 33, 44)`,
      backgroundPosition: 'center center',
      backgroundBlendMode: 'overlay',
      backgroundSize: 'cover'
    };
  }
  return baseStyle;
};
