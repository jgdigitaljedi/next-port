import fs from 'fs';
import matter from 'gray-matter';
// @ts-ignore
import mdxPrism from 'mdx-prism';
import { GetStaticPaths, GetStaticProps } from 'next';
import { serialize } from 'next-mdx-remote/serialize';
import { MDXRemote, MDXRemoteSerializeResult } from 'next-mdx-remote';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import path from 'path';
import React from 'react';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import Layout, { WEBSITE_HOST_URL } from '../../../components/blog/layout';
import { MetaProps } from '../../../types/layout';
import { NextPrev, PostType, Section } from '../../../types/post';
import { postFilePaths, POSTS_PATH } from '../../../util/mdxUtils';
import PostTitle from '../../../components/postTitle/postTitle';
import { Box, Flex, Text } from 'theme-ui';
import { formatDate } from '../../../util/helpers';
import readingTime from 'reading-time';
import { FaRegCalendarAlt, FaRegClock, FaTags } from 'react-icons/fa';
import ImageBox from '../../../components/imageBox';
import Breadcrumbs from '../../../components/blog/breadcrumbs';
import BlogTable from '../../../components/blog/blogTable';
import { getNextPrevious } from '../../../lib/getMdx';
import NextPrevSection from '../../../blog/nextPrevSection';

const components = {
  Head,
  Image,
  Link,
  PostTitle,
  ImageBox,
  BlogTable
};

type PostPageProps = {
  source: MDXRemoteSerializeResult;
  frontMatter: PostType;
  readTime: string;
  nextPrev: NextPrev;
};

const PostPage = ({ source, frontMatter, readTime, nextPrev }: PostPageProps): JSX.Element => {
  const customMeta: MetaProps = {
    title: `${frontMatter.title} - Hunter Chang`,
    description: frontMatter.description,
    image: `${WEBSITE_HOST_URL}${frontMatter.image}`,
    date: frontMatter.date,
    type: 'article'
  };
  return (
    <Layout customMeta={customMeta}>
      <Box as="article">
        <Breadcrumbs section={frontMatter.category as Section} />
        <PostTitle title={frontMatter.title} top={true} />
        <Flex
          sx={{
            flexDirection: ['column', 'column', 'column', 'row'],
            justifyContent: 'space-evenly'
          }}
        >
          <Flex sx={{ alignItems: 'center' }}>
            <FaRegCalendarAlt sx={{ mr: 2 }} />
            {formatDate(frontMatter?.date || '')}
          </Flex>
          <Flex sx={{ alignItems: 'center' }}>
            <FaRegClock sx={{ mr: 2 }} />
            {readTime}
          </Flex>
          {frontMatter.tags && (
            <Flex sx={{ alignItems: 'center' }}>
              <FaTags sx={{ mr: 2 }} />
              {frontMatter.tags.join(', ')}
            </Flex>
          )}
        </Flex>
        <Box>
          {frontMatter.image && (
            <Box sx={{ mt: 4 }}>
              <ImageBox
                imgPath={frontMatter.image}
                altText={frontMatter.altText || 'unknown'}
                maxiWidth="48rem"
              />
            </Box>
          )}
          <MDXRemote {...source} components={components} />
          <NextPrevSection nextPrev={nextPrev} section={frontMatter.section} />
        </Box>
      </Box>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const postFilePath = path.join(POSTS_PATH, `${params?.category}/${params?.slug}.mdx`);
  const source = fs.readFileSync(postFilePath);

  const { content, data } = matter(source);
  const rt = readingTime(content);

  const mdxSource = await serialize(content, {
    // Optionally pass remark/rehype plugins
    mdxOptions: {
      remarkPlugins: [require('remark-code-titles')],
      rehypePlugins: [mdxPrism, rehypeSlug, rehypeAutolinkHeadings]
    },
    scope: data
  });

  const nextPrev = getNextPrevious(data?.section, params?.slug as string);

  return {
    props: {
      source: mdxSource,
      frontMatter: data,
      readTime: rt.text,
      nextPrev
    }
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const paths = postFilePaths
    // Remove file extensions for page paths
    .map((path) => path.replace(/\.mdx?$/, ''))
    // Map the path into the static paths object required by Next.js
    .map((slug) => {
      const sSplit = slug.split('/');
      return { params: { slug: sSplit[1], category: sSplit[0] } };
    });

  return {
    paths,
    fallback: false
  };
};

export default PostPage;
