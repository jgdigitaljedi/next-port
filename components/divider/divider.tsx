import React from 'react';
import { Box } from 'theme-ui';

const Divider: React.FC = () => {
  return (
    <Box
      sx={{
        borderBottomWidth: '3px',
        borderBottomStyle: 'dotted',
        borderBottomColor: 'primary',
        width: '100%',
        my: 4
      }}
    ></Box>
  );
};

export default Divider;
