import { Feed } from 'feed';
import { writeFileSync, readdirSync, readFileSync } from 'fs';
import { join } from 'path';
import matter from 'gray-matter';

const POSTS_PATH = join(process.cwd(), 'blog');
const defaultFields = ['date', 'description', 'slug', 'title', 'category', 'image', 'altText'];

async function getPostBySlug(slug, section, fields = []) {
  const realSlug = slug.replace(/\.mdx$/, '');
  const fullPath = join(POSTS_PATH, section, `${realSlug}.mdx`);
  const fileContents = readFileSync(fullPath, 'utf8');
  const { data, content } = matter(fileContents);

  const items = {};

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug;
    }
    if (field === 'content') {
      items[field] = rendered.default;
    }
    if (data[field]) {
      items[field] = data[field];
    }
  });

  items.content = rendered.default;

  return items;
}

const getSectionPosts = (section) => {
  return readdirSync(join(POSTS_PATH, section))
    .map((file) => `${file}`)
    .filter((path) => /\.mdx?$/.test(path));
};

const getPostsForSection = async (section) => {
  const sectionPosts = getSectionPosts(section);
  const slugs = sectionPosts.map((slug) => {
    return { slug: `${slug}`, section: section };
  });
  const posts = slugs
    .map(async (obj) => await getPostBySlug(obj.slug, obj.section, defaultFields))
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
  return await posts;
};

const generateRSSFeed = (articles, section) => {
  const baseUrl = `https://joeyg.me/blog/${section}`;
  const author = {
    name: 'Joey Gauthier',
    email: 'joey@joeyg.me',
    link: ''
  };

  // Construct a new Feed object
  const feed = new Feed({
    title: 'Articles by Joey Gauthier',
    description: 'Software engineering and video game articles from the mind of Joey Gauthier.',
    id: baseUrl,
    link: baseUrl,
    language: 'en',
    feedLinks: {
      rss2: `${baseUrl}/rss.xml`
    },
    author,
    copyright: '©Joey Gauthier 2022'
  });

  // Add each article to the feed
  articles.forEach((post) => {
    console.log('post', post);
    const {
      content,
      fileName,
      meta: { date, description, title }
    } = post;
    const url = `${baseUrl}/${fileName}`;

    feed.addItem({
      title,
      id: url,
      link: url,
      description,
      content,
      author: [author],
      date: new Date(date)
    });
  });

  // Write the RSS output to a public file, making it
  // accessible at joeyg.me/rss.xml
  writeFileSync('public/rss.xml', feed.rss2());
};

const collectDataForFeed = async (section) => {
  const slugs = await getPostsForSection(section);
  Promise.all(slugs).then((result) => {
    console.log('result', result);
  });

  // console.log('slugs', slugs);
};

export const buildFeeds = async () => {
  await collectDataForFeed('coding');
  await collectDataForFeed('gaming');
};

// export default buildFeeds;
