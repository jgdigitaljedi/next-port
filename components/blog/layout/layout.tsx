import React from 'react';
import { Box } from 'theme-ui';
import { MetaProps } from '../../../types/layout';
import Head from '../head';

type LayoutProps = {
  children: React.ReactNode;
  customMeta?: MetaProps;
};

export const WEBSITE_HOST_URL = 'https://nextjs-typescript-mdx-blog.vercel.app';

const Layout = ({ children, customMeta }: LayoutProps): JSX.Element => {
  return (
    <>
      <Head customMeta={customMeta} />
      <Box as="main">
        <Box>{children}</Box>
      </Box>
    </>
  );
};

export default Layout;
