import bottomOfCOnsoleSHelves from '../public/static/bottomFoConsolesShelves.jpg';
import boxesShelf1 from '../public/static/boxesShelf1.jpg';
import boxesShelf2 from '../public/static/boxesShelf2.jpg';
import cgf2022Day1 from '../public/static/cgf2022Dat1.jpg';
import controllers1 from '../public/static/controllers1.jpg';
import crt from '../public/static/crt.jpg';
import topOfConsolesShelves from '../public/static/topOfConsolesShelves.jpg';

export const vgGalleryPics = [
  {
    image: bottomOfCOnsoleSHelves,
    title: 'Bottom area of console shelves'
  },
  {
    image: boxesShelf1,
    title: 'Shelf of console and game boxes 1'
  },
  {
    image: boxesShelf2,
    title: 'Shelf of console and game boxes 2'
  },
  {
    image: cgf2022Day1,
    title: 'Classic Game Fest 2022 day 1 haul'
  },
  {
    image: controllers1,
    title: 'N64 games drawer and some controllers'
  },
  {
    image: crt,
    title: '36 inch Sony Trinitron CRT TV'
  },
  {
    image: topOfConsolesShelves,
    title: 'Top area of console shelves'
  }
];
