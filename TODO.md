# TODO

- blog posts
  - blog post template needs finished
  - comments?
  - write first couple posts
- blog posts sections
  - needs pagination
  - search?
- sections
  - showcase
    - images to projects
  - bio
    - style of job cards needs work
      - maybe do more of a timeline presentation and scrap the boring cards
- overall
  - improve style
  - a11y audit
  - setting to turn off motion?
- util
  - GDPR?
  - rss feed generation
