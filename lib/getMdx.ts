import fs, { readFileSync } from 'fs';
import matter from 'gray-matter';
import { serialize } from 'next-mdx-remote/serialize';
import { join } from 'path';
import { PostType, Section } from '../types/post';
import { getSectionPosts, postFilePaths, POSTS_PATH } from '../util/mdxUtils';
// @ts-ignore
import mdxPrism from 'mdx-prism';
import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';

const defaultFields = ['date', 'description', 'slug', 'title', 'category', 'image', 'altText'];

interface PostAmount {
  startIndex: number;
  num: number;
}

// const postsPaths: string[] = blogSubdirs.map((subdir: string) => {
//   return join(POSTS_PATH, subdir);
// });

export function getPostSlugs(): string[] {
  // return fs.readdirSync(POSTS_PATH);
  return postFilePaths;
}

type PostItems = {
  [key: string]: string;
};

export function getPostBySlug(slug: string, section: string, fields: string[] = []): PostItems {
  const realSlug = slug.replace(/\.mdx$/, '');
  const fullPath = join(POSTS_PATH, section, `${realSlug}.mdx`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');
  const { data, content } = matter(fileContents);

  const items: PostItems = {};

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug;
    }
    if (field === 'content') {
      items[field] = content;
    }
    if (data[field]) {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllPosts(fields: string[] = []): PostItems[] {
  const slugs = getPostSlugs().map((slug: string) => {
    const sSplit = slug.split('/');
    return { slug: sSplit[sSplit.length - 1], section: sSplit[sSplit.length - 2] };
  });
  const posts = slugs
    .map((obj) => getPostBySlug(obj.slug, obj.section, fields))
    // sort posts by date in descending order
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
  return posts;
}

export function getPostsForSection(section: Section, amount?: PostAmount): PostItems[] {
  const sectionPosts = getSectionPosts(section);
  const slugs = sectionPosts.map((slug: string) => {
    return { slug: `${slug}`, section: section };
  });
  const posts = slugs
    .map((obj) => getPostBySlug(obj.slug, obj.section, defaultFields))
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
  if (amount) {
    const selected = posts.slice(amount.startIndex, amount.num + amount.startIndex);
    return selected;
  }
  return posts;
}

async function getFullPostData(posts: PostItems[], section: Section) {
  const postsWithPaths = posts.map(async (post) => {
    const filePath = join(POSTS_PATH, `${section}/${post?.slug}.mdx`);
    const source = readFileSync(filePath);
    const { content } = matter(source);
    const mdxSource = await serialize(content, {
      mdxOptions: {
        remarkPlugins: [require('remark-code-titles')],
        rehypePlugins: [mdxPrism, rehypeSlug, rehypeAutolinkHeadings]
      },
      scope: post
    });
    return { ...post, filePath, content: mdxSource };
  });
  return Promise.all(postsWithPaths);
}

const getData = (section: Section, slug: string) => {
  const postFilePath = join(POSTS_PATH, `${section}/${slug}.mdx`);
  const source = fs.readFileSync(postFilePath);

  const { data } = matter(source);
  return { data, slug };
};

export function getNextPrevious(section?: Section, slug?: string) {
  if (section && slug) {
    const sectionPosts = getSectionPosts(section);
    const extRemoved = sectionPosts.map((slug: string) => slug.split('.')[0]);
    const currentIndex = extRemoved.indexOf(slug);
    const sLast = extRemoved.length - 1;
    return {
      next: currentIndex < sLast ? getData(section, extRemoved[currentIndex + 1]) : null,
      prev: currentIndex === 0 ? null : getData(section, extRemoved[currentIndex - 1])
    };
  } else {
    return { next: null, prev: null };
  }
}
