import Image from 'next/image';
import React from 'react';
import { Box, Card, Flex, Heading, Text } from 'theme-ui';
import { JobProps } from '../../util/constants';

interface JobCardProps {
  job: JobProps;
}

const JobCard: React.FC<JobCardProps> = ({ job }) => {
  return (
    <Card variant="job">
      <Flex
        sx={{
          mb: 3,
          justifyContent: 'center',
          alignItems: 'center',
          borderBottomColor: 'text',
          borderBottomStyle: 'solid',
          borderBottomWidth: '1px',
          pb: 3
        }}
      >
        {job.logo && <Image src={job.logo} width={80} height={80} alt={`${job.company} logo`} />}
        <Box sx={{ ml: 3 }}>
          <Heading as="h3" sx={{ color: 'primary' }}>
            {job.company}
          </Heading>
          <Box>
            {job.position} - <Text sx={{ fontStyle: 'italic' }}>{job.team}</Text>
          </Box>
          <Box>{job.dates}</Box>
        </Box>
      </Flex>
      <Box
        sx={{
          borderBottomColor: 'text',
          borderBottomStyle: 'solid',
          borderBottomWidth: '1px',
          pb: 3,
          mb: 3
        }}
      >
        {job.description.map((desc: string, index: number) => (
          <Box as="p" sx={{ mt: 2 }} key={job.company + '-' + index}>
            {desc}
          </Box>
        ))}
      </Box>
      <Box>
        <Text sx={{ fontWeight: 'bold', color: 'primary' }}>Tech used: </Text>
        <Text sx={{ fontStyle: 'italic', color: 'primary' }}>{job.tech.join(', ')}</Text>
      </Box>
    </Card>
  );
};

export default JobCard;
