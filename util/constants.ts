import indeedLogo from '../public/static/indeedLogo.jpeg';
import r4Logo from '../public/static/r4Logo.jpg';
import kapschLogo from '../public/static/kapschLogo.jpeg';
import placeholderImg from '../public/static/placeholder.jpg';

export interface NavItems {
  title: string;
  link: string;
}

export interface JobProps {
  company: string;
  logo: any;
  dates: string;
  position: string;
  description: string[];
  tech: string[];
  team?: string;
}

export const blogSubdirs: string[] = ['coding', 'gaming'];

export const navItems: NavItems[] = [
  {
    title: 'Bio',
    link: '/bio'
  },
  {
    title: 'Hobbies',
    link: '/hobbies'
  },
  {
    title: 'Showcase',
    link: '/showcase'
  },
  {
    title: 'Blog',
    link: '/blog'
  }
];

export const jobs: JobProps[] = [
  {
    company: 'Indeed',
    position: 'UX Developer',
    team: 'Platform (UX Quality)',
    dates: 'Jan 2020 - present',
    tech: [
      'React',
      'Node',
      'TypeScript',
      'Styled Components',
      'Redux',
      'Federated Modules',
      'Jest',
      'TestCafe',
      'Storybook',
      'Chromatic',
      'GitLab CI',
      'Jenkins',
      'Apollo',
      'GraphQL',
      'FullStory'
    ],
    logo: indeedLogo,
    description: [
      `Indeed has been a place where I can honestly say I've seen the most growth as a developer even though, before starting at Indeed, I was convinced that I didn't have much room to grow. When I initially joined Indeed, I was on a product team that built our products on the company's plugin framework using global and product Redux stores to pass around data. As time has progressed, we have moved to a new architecture using federated modules and a highly refined internal React component framework built on top of Theme-UI.ndeed has been a place where I can honestly say I’ve seen the most growth as a developer even though, before starting at Indeed, I was convinced that I had little room to grow. When I initially joined Indeed, I was on a product team that built our products on the company’s plugin framework using global and product Redux stores to pass around data. As time has progressed, we have moved to a new architecture using federated modules, and a highly refined internal React component framework built on top of Theme-UI.`,
      `Aside from just writing front-end code, a UX Developer has to stay informed of the recent changes in technology and guidelines within the company and make sure that the rest of your team is adhering to those standards and practices. When accessibility became a much larger concern, it was also my duty to ensure that my teammates were aware of the new tooling and practices the company was using to make sure we were creating the most accessible experience possible.`,
      `In June 2022, I moved to the Platform team and am now working with a small group of developers and designers who are trying to improve the overall experience of Indeed.com by coming up with a richer design language to ensure a consistent experience across all products, create and research tooling to be used by developers to better enable them to provide a fully accessible experience, and creating additional customer-facing views that are not large enough in scope to need a full product team to maintain them. I’ve recently completed an audit of the entire site to give the results to one of our lead designers so we can work together to enhance our design language. I also develop more tooling, including an internal eslint plugin, a tool to search a code base for untranslated strings, a tool to query user feedback from the site and auto generate Jira tickets to address that feedback, and much more.`
    ]
  },
  {
    company: 'SnagApple',
    logo: placeholderImg,
    position: 'Front End Engineer/Co-founder',
    team: 'Founders',
    dates: 'Jun 2017 - Nov 2018',
    tech: ['Angular', 'SCSS', 'Jenkins', 'TypeScript', 'Node', 'ExpressJS', 'MariaDB'],
    description: [
      'When you hear stories of software engineers joining a startup as a side hustle and hoping it is their big ticket to becoming wealthy, you can think of my experience with this company in that same light. I honestly don’t know what the real name of the company is at this point, but I know it is no longer SnagApple as my friend who introduced me to this company told me that Apple threatened to sue them over their name shortly after I left so they changed it. Regardless, I gave a year of my free time to this company to get it off the ground. SnagApple was a company that was trying to disrupt the real estate industry by creating a product for realtors that would allow them to instantly generate a website with all of their branding and local MLS data. They would also get an administrative portal where they could message potential buyers and sellers, change listings, and see analytic data to let them know how often certain properties were being viewed and for how long.',
      ' I was the fifth person to join SnagApple and did so as their third software engineer and their last of the five co-founders. We met weekly to discuss our business and development strategies and were led by the main founder, who was also a bit of a real estate mogul. My primary duties were to assist the other front-end developer with the principal product, develop the back end for the admin portal, and help onboard and manage developers working as contractors.',
      'After a year of waking up extra early and signing back into my laptop extra late to work on SnagApple’s product, I got a bit burnt out and lost some faith in what we were working on creating. Some other companies with similar ideas had beaten us to the market, and I reached a point where I was ready to work on my own projects again in my spare time once again. I ultimately forfeited my 12% stake in the company and resigned from my position. I learned a lot about business while continuing to level up my craft, so I still view this as a positive experience.'
    ]
  },
  {
    company: 'R4 Technologies',
    position: 'Senior Frontend Engineer',
    team: 'Front End Team',
    dates: 'Nov 2016 - Jan 2020',
    tech: [
      'Angular',
      'SCSS',
      'RXJS',
      'D3',
      'Node',
      'AWS',
      'Jenkins',
      'TypeScript',
      'Mocha',
      'Jasmine',
      'Webpack',
      'Mongo'
    ],
    logo: r4Logo,
    description: [
      'R4 Technologies is a company that specializes in taking heaps of data from various companies, running it all through their custom engine, and creating new ways to use the data to improve their client’s business. Sometimes this can mean that R4 developed a suggested product section for an eCommerce company that would base recommendations on previously viewed items. Other times, this meant that R4 created rich dashboards full of graphs, charts, tables, and various other types of visualizations that would help the companies get the most from their data.',
      'R4 is a smaller company, and the front-end team comprised myself and 3 other people. Our team not only had to do all the front-end development work for the company’s products, but we also migrated their previous front-end work to Angular and had a say in other technologies the company used.',
      'Once we migrated the company’s code bases to Angular, we spent a lot of time developing our own state management approach leaning heavily onto RXJS, creating rich and interactive visualizations in a reusable fashion, developing the company’s internal design language, and so much more. When you are one of four people in a company doing all the front-end development and decision making, you certainly have to wear many hats!'
    ]
  },
  {
    company: 'Kapsch TrafficCom',
    logo: kapschLogo,
    position: 'Front End Developer',
    team: 'Front End Team',
    dates: 'Oct 2014 - Nov 2016',
    tech: [
      'AngularJS',
      'LESS',
      'Jenkins',
      'JavaScript',
      'Python',
      'Node',
      'Google Maps API',
      'SOAP'
    ],
    description: [
      'My first professional software development position was at Kapsch TrafficCom (Schneider Electric at the time they hired me). Their director scouted me at my open house presentation at the end of my time attending MakerSquare. The group of people I worked with at Kapsch designed tolling solutions for different cities and the dashboards that allowed them to monitor them and change the pricing.',
      'The group of people I worked with included hardware-focused people that were always seeking the best sensors, cameras, etc to place on the roads; back-end developers that wrote the code, which allowed all the devices to communicate with our servers and the stored procedures that allowed the client applications to consume that data; and my team of front end engineers that wrote the various clients and were tasked with maintaining the legacy FLEX (ActionScript) client and python backend code. Kapsch had been developing their client software for the desktop since its inception, but they had decided it was time to move their client software to the web, and they had hired my small team to make that a reality for them.'
    ]
  }
];
