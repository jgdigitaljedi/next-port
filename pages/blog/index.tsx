import React from 'react';
import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { Box, Flex, Text } from 'theme-ui';
import { containerStyle } from '../../styles/global.style';
import PostTitle from '../../components/postTitle/postTitle';
import { getPostsForSection } from '../../lib/getMdx';
import { MainBlogProps } from '../../types/post';
import RecentCard from '../../components/blog/recentCard';
import CodingBg from '../../public/static/codingBgOverlay.png';
import GamingBg from '../../public/static/gamingBgOverlay.png';

const Blog: NextPage<MainBlogProps> = ({ recentCoding, recentGaming }) => {
  return (
    <Box sx={containerStyle}>
      <Head>
        <title>Joey Gauthier | Blog</title>
        <meta name="description" content="Joey's Blog" />
        <meta
          name="keywords"
          content="Joey Gauthier, blog, retro gaming, frontend, React, coding blog"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box>
        <PostTitle title="Joey's blogs" />
        <Text as="p" sx={{ mt: 4 }}>
          I haven't had a blog in a while and I decided I might enjoy having an outlet for writing
          once again. It makes the most sense to blog about something you love, so I decided to host
          a gaming/retro game collecting blog alongside a blog about software engeering.
        </Text>
        <Flex
          sx={{
            mt: 4,
            justifyContent: 'center',
            // width: ['100%', '100%', '100%', '100%', 'auto'],
            flex: 1,
            flexDirection: ['column', 'column', 'column', 'column', 'row']
          }}
        >
          <RecentCard
            entry={recentCoding}
            sectionLink="/blog/coding"
            sectionLinkSection="Coding"
            recentText={true}
            marginRight={true}
            sectionImage={CodingBg}
          />
          <RecentCard
            entry={recentGaming}
            sectionLink="/blog/gaming"
            sectionLinkSection="Gaming"
            recentText={true}
            sectionImage={GamingBg}
          />
        </Flex>
      </Box>
    </Box>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const recentCoding = getPostsForSection('coding', { num: 1, startIndex: 0 })[0];
  const recentGaming = getPostsForSection('gaming', { num: 1, startIndex: 0 })[0];

  return {
    props: { recentCoding, recentGaming }
  };
};

export default Blog;
