import React from 'react';
import { Box } from 'theme-ui';
import { tableBodyStriped, tableContainerStyle, tableOuterStyle } from './blogTable.style';

interface BlogTableProps {
  header: string[];
  rows: string[][];
  footer?: string[];
  centered?: boolean;
}

// indexes used because these will be static so index won't change
const BlogTable: React.FC<BlogTableProps> = ({ header, rows, footer, centered }) => {
  return (
    <Box sx={tableContainerStyle(centered)}>
      <Box as="table" sx={tableOuterStyle}>
        <Box as="thead">
          <Box as="tr" sx={{ color: 'background' }}>
            {header.map((colHead: string, index: number) => {
              return (
                <Box as="th" key={`head-${index}`}>
                  {colHead}
                </Box>
              );
            })}
          </Box>
        </Box>
        <Box as="tbody" sx={tableBodyStriped}>
          {rows.map((rowData: string[], index: number) => {
            return (
              <Box as="tr" key={`row-${index}`}>
                {rowData.map((item: string, ind: number) => {
                  return (
                    <Box as="td" key={`item-${index}-${ind}`}>
                      {item}
                    </Box>
                  );
                })}
              </Box>
            );
          })}
        </Box>
        {footer && (
          <Box as="tfoot">
            <Box as="tr" sx={{ fontWeight: 'bold' }}>
              {footer.map((foot: string, index: number) => (
                <Box as="td" key={`foot-${index}`}>
                  {foot}
                </Box>
              ))}
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default BlogTable;
