import React from 'react';
import { Link, NavLink } from 'theme-ui';
import { NavItems, navItems } from '../../../util/constants';

interface NavItemProps {
  currentPath: string;
}

const NavItem: React.FC<NavItemProps> = ({ currentPath }) => {
  return (
    <>
      {navItems.map((item: NavItems) => {
        const isCurrent =
          currentPath === item.link || (item.title === 'Blog' && currentPath.indexOf('blog') >= 0);
        return (
          <NavLink
            sx={{
              bg: isCurrent ? 'primary' : 'muted',
              color: isCurrent ? 'background' : 'text',
              p: 2,
              mr: 4,
              borderRight: '1px solid white',
              '&:hover': {
                bg: 'background'
              }
            }}
            href={item.link}
            key={`nav-${item.title}`}
          >
            {'> ' + item.title}
          </NavLink>
        );
      })}
    </>
  );
};

export default NavItem;
