/** @type {import('next-sitemap').IConfig} */
module.exports = {
  siteUrl: process.env.SITE_URL || 'https://joeyg.me',
  generateRobotsTxt: true,
  generateIndexSitemap: false
};
