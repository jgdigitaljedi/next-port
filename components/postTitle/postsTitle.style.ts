import { ThemeUIStyleObject } from 'theme-ui';

export const postTitleStyle: ThemeUIStyleObject = {
  borderBottomWidth: '3px',
  borderBottomStyle: 'dotted',
  borderBottomColor: 'primary',
  color: 'primary',
  m: '0 0 15px',
  pb: '15px',
  position: 'relative',
  display: 'flex',
  alignItems: 'center',
  '&:after': {
    borderBottomWidth: '3px',
    borderBottomStyle: 'dotted',
    borderBottomColor: 'primary',
    bottom: '2px',
    content: '""',
    display: 'block',
    position: 'absolute',
    width: '100%'
  },
  '::first-letter': {
    textTransform: 'capitalize'
  }
};
