export enum LinkType {
  GitHub,
  GitLab,
  Site,
  Npm
}

export interface ShowCase {
  title: string;
  image: any;
  description: string;
  link: string;
  linkType: LinkType | null;
  tech: string[];
}

export const showcaseProjects: ShowCase[] = [
  {
    title: 'My portfolio (this site)',
    image: null,
    description:
      'In all honesty, I’m sort of done with the idea of spending a ton of time building an extravagant portfolio site that rarely gets viewed and takes me away from working on other projects. I originally set out to create this site using HUGO and found a theme called Terminal that seemed nice. After 3 days of work, I realized the theme was a little old and didn’t seem to work properly with HUGO. Couple that with the fact that I’ve never used HUGO before, which meant I wasn’t knowledgeable on how to debug it, and I opted to reach for Next.js for the sake of shipping this quickly. I had never used Next.js before, but it uses React with which I’m quite familiar, so I figured the turnaround time would still be rather quick.',
    link: 'https://gitlab.com/jgdigitaljedi/next-port',
    linkType: LinkType.GitLab,
    tech: ['Next.js', 'Theme-UI', 'TypeScript']
  },
  {
    title: 'New Sweden Lutheran Church website',
    image: null,
    description:
      'I’ve gone through most of my adult life without giving thought to the idea of spirituality, but I grew up in a church and eventually decided I wanted my son to be exposed to religion so he could make an informed decision on his own spiritual beliefs later in life. My family and I joined New Sweden Lutheran Church. As usual, I checked out their website one day and realized that they desperately needed something better. I made it known that I could build a new site for them and, eventually, they asked me if I would do so. Since it needed to be maintained by non-developer types, I did something I was always somewhat against doing and reached for WordPress. This new site currently isn’t live, but I will update the link here as soon as I launch it. ',
    link: '',
    linkType: null, // change this to Site as soon as launched
    tech: ['WordPress', 'Linode', 'PHP']
  },
  {
    title: 'Video Games Collection App',
    image: null,
    description:
      'When I joined Indeed, I had zero experience using React. Up to this point, I had worked at Angular shops and explored Vue in my spare time, but I had honestly avoided using React because Facebook built it and they are a company for which I have nothing nice to say. In order to learn React quickly, I threw up this repo and built a video game collection app so I could easily monitor and search my collection. In the beginning, I was using bash scripts to import data from a previous version I built in Vue. I was also experimenting with class components, function components, Redux, context, hooks, and more so, the code was a bit all over the place. What started as a learning tool became something I used regularly. That said, I only ever proceeded forward with features and never went back to clean up the code from when I was learning, so don’t judge me by this code base!',
    link: 'https://github.com/jgdigitaljedi/games-library',
    linkType: LinkType.GitHub,
    tech: ['React', 'Node', 'Express', 'Primereact', 'TypeScript']
  },
  {
    title: 'Video Games Supplemental Data',
    image: null,
    description:
      'Once I got into collecting retro video games, I started building apps that consume data from video game APIs. The data that IGDB and Giantbomb provide are great, but once I dove deeper into this hobby, I realized that there was a lot of data these APIs did not have. Data points like launch titles, platform exclusives, games banned internationally, etc were points of interest for me but were not a part of any API. I created this repo which is full of scripts to scrape and format other websites to collect these sorts of data points. The app includes a front end, which allows me to find the IDs of games and consoles from the aforementioned APIs and associate those IDs with the data I am collecting. The result is a series of JSON files that can be used in conjunction with these 2 APIs to add another layer of interesting data to any video games app.',
    link: 'https://github.com/jgdigitaljedi/video-game-db-supplemental-data',
    linkType: LinkType.GitHub,
    tech: ['Node', 'Vue', 'Express']
  },
  {
    title: 'stringman-utils',
    image: null,
    description:
      'Stringman-utils is a library I’ve published which was inspired by the idea that developers commonly need to look up regular expressions and other types of string manipulation which aren’t used often enough to commit to memory. The name is a play on words as it is short for “string manipulation utilities” and I am a guitar player or “string man”. If you need to look for an email address within a string, there’s a method for that. If you need to check that an URL is a valid URL, there’s a method for that. As of right now, there are 320 of these kinds of methods exposed in this package to make these sorts of operations simpler.',
    link: 'https://www.npmjs.com/package/stringman-utils',
    linkType: LinkType.Npm,
    tech: ['Node', 'TypeScript', 'Big.js']
  },
  {
    title: 'GS Scraper',
    image: null,
    description:
      'GS-Scraper is another little project inspired by my love for retro video game collecting. The app essentially scrapes data from many of the popular classifieds websites allowing you to search all of them at the same time. The results include the price, images, listing title, and original link to the items found so you can easily get the information you need to purchase the item. You can even add results to your “favorites” which can be viewed on a different screen if you want to track items to purchase at a later date. This is currently my most starred repo on GitHub.',
    link: 'https://github.com/jgdigitaljedi/gs-scraper',
    linkType: LinkType.GitHub,
    tech: ['Node', 'Express', 'Vue', 'Cheerio']
  },
  {
    title: 'Texas Lassos social network',
    image: null,
    description:
      'As part of attending MakerSquare, I was expected to spend the last 2 weeks working on 1-2 graduation projects to show off at the open house, which really served as a recruiting event. The Texas Lassos social network was the first of my 2 projects and I spent 8 days building it with 2 other students. We were basically attempting to build a simplified version of Facebook themed with the Texas Lassos colors that could be used as a private social network for the group. The app is still in use by the Lassos today. Obviously, it’s a private app so I can’t link the code base or a link to the app here.',
    link: '',
    linkType: null,
    tech: ['Ruby', 'Rails', 'Bootstrap', 'Postgres']
  }
];
