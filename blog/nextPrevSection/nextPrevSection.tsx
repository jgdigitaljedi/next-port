import Link from 'next/link';
import React from 'react';
import { FaLongArrowAltLeft, FaLongArrowAltRight } from 'react-icons/fa';
import { Card, Flex, Text } from 'theme-ui';
import { NextPrev, Section } from '../../types/post';
import { cardStyle } from './nextPrevSection.style';

interface NextPrevSectionProps {
  nextPrev: NextPrev;
  section: Section;
}

const NextPrevSection: React.FC<NextPrevSectionProps> = ({ nextPrev, section }) => {
  console.log('nextPrev', nextPrev);
  return (
    <Flex
      sx={{
        flexDirection: ['column', 'column', 'column', 'row'],
        alignItems: 'center',
        justifyContent: 'center',
        mt: 4
      }}
    >
      {nextPrev.prev && (
        <Link as={`/blog/${section}/${nextPrev.prev.slug}`} href="/blog/[category]/[slug]">
          <Card variant="project" sx={{ ...cardStyle(nextPrev.prev.data.image), mr: 3 }}>
            <FaLongArrowAltLeft sx={{ mr: 2 }} />
            <Text>{nextPrev.prev.data.title}</Text>
          </Card>
        </Link>
      )}
      {nextPrev.next && (
        <Link as={`/blog/${section}/${nextPrev.next.slug}`} href="/blog/[category]/[slug]">
          <Card variant="project" sx={cardStyle(nextPrev.next.data.image)}>
            <Text>{nextPrev.next.data.title}</Text>
            <FaLongArrowAltRight sx={{ ml: 2 }} />
          </Card>
        </Link>
      )}
    </Flex>
  );
};

export default NextPrevSection;
