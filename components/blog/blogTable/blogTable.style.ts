import { ThemeUIStyleObject } from 'theme-ui';

export const tableContainerStyle = (centered?: boolean): ThemeUIStyleObject => {
  if (centered) {
    return {
      width: '100%',
      display: 'flex',
      justifyContent: 'center'
    };
  }
  return {};
};

export const tableOuterStyle: ThemeUIStyleObject = {
  borderCollapse: 'collapse',
  'th, td': {
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: 'highlight',
    textAlign: 'left',
    p: 2
  },
  'thead tr th, tfoot tr td': {
    backgroundColor: 'white',
    color: 'background'
  }
};

export const tableBodyStriped: ThemeUIStyleObject = {
  'tr:nth-of-type(odd)': {
    backgroundColor: 'bgLighter'
  }
};
