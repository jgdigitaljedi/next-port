import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { Box } from 'theme-ui';
import SectionLayout from '../../components/blog/sectionLayout';
import { getPostsForSection } from '../../lib/getMdx';
import { IndexProps } from '../../types/post';

const GamingIndex: NextPage<IndexProps> = ({ posts }) => {
  const topText = `For me, there is a lot more to gaming than just sitting down to play a game. Maybe I’m on a mission to find the next “white whale” for my retro video games collection. Maybe I’ve gotten out the soldering iron to modify and/or repair an old video games console. Regardless, if I want to post something about gaming, you will find it here.`;
  return (
    <Box>
      <Head>
        <title>Joey Gauthier's coding blog</title>
        <meta name="description" content="Joey Gauthier's gaming blog" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SectionLayout posts={posts} section="gaming" topText={topText} />
    </Box>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const posts = getPostsForSection('gaming', { startIndex: 0, num: 3 });

  return {
    props: { posts }
  };
};

export default GamingIndex;
