import React from 'react';
import { Box, Grid } from 'theme-ui';
import { containerStyle } from '../../../styles/global.style';
import { PostType, Section } from '../../../types/post';
import PostTitle from '../../postTitle/postTitle';
import RecentCard from '../recentCard';

interface SectionLayoutProps {
  posts: PostType[];
  section: Section;
  topText: string;
}

const SectionLayout: React.FC<SectionLayoutProps> = ({ posts, section, topText }) => {
  return (
    <Box sx={containerStyle} as="main">
      <PostTitle title={`${section} blog`} />
      <Box sx={{ my: 4 }}>{topText}</Box>
      <Grid gap={[2, 2, 2, 3, 4]} columns={[1, 1, 1, 2, 3, 4]}>
        {posts.map((post) => (
          <RecentCard entry={post} key={post.slug} sectionLinkSection={section} />
        ))}
      </Grid>
    </Box>
  );
};

export default SectionLayout;
