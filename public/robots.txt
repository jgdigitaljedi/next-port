# *
User-agent: *
Allow: /

# Host
Host: https://joeyg.me

# Sitemaps
Sitemap: https://joeyg.me/sitemap.xml
